extends Node


# items are in a specific group
func _ready():
	add_to_group("items")


# signal when something enters in collision whith that item
func _on_Key_body_entered(body_):
	# enemies could also pickup items
	# then they'll have to be killed to drop the items
	if body_.has_method("pickUp"):
		body_.pickUp(self)
		# remove from scene
		queue_free()
