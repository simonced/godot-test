extends Node

# direction shortcuts
const idle  = Vector2(0, 0)
const up    = Vector2(0, -1)
const right = Vector2(1,  0)
const down  = Vector2(0,  1)
const left  = Vector2(-1, 0)
