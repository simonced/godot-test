
Game structure:
===============

Maps:
-----

- game (Node, root of the tree)
   |
   ` player (instance of player scene, careful, in lower case!)

The first Node should be named "game" and the player instance "player".
This is because the player node will be referenced in different scripts.


Tilesets / sprites resources:
=============================

- https://0x72.itch.io/dungeontileset-ii
- https://assetstore.unity.com/packages/2d/environments/2d-pixel-top-down-dungeon-tileset-73645
- https://opengameart.org/content/kyrises-free-16x16-rpg-icon-pack
