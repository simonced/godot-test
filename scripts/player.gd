extends KinematicBody2D

# inventory is a separate script
onready var inventory = preload("res://scripts/inventory.gd").new()

# custom signals
signal hp_changed

# player data
const SPEED  = 128
var move_dir  = Direction.down
var sprite_dir = "down"
onready var facing_ray = $facing_ray
var is_pressing_action = false
var hp = 100

# ====================
# called by the engine
func _physics_process(delta):
	controls_loop()
	movement_loop()

	# orienting the sprite
	sprite_loop()
	# orienting the ray (for detection)
	facing_ray_loop()

	if is_pressing_action:
		# activable in front?
		activate_in_front()

	# moving?
	if move_dir == Direction.idle:
		anim_switch("idle")
	else:
		anim_switch("walk")


# ===========================================================
# checking controls and updating the data related to movement
# will set the movement direction (move_dir)
func controls_loop():
	# movement keys
	var LEFT  = Input.is_action_pressed("ui_left") # those are bools?
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP    = Input.is_action_pressed("ui_up")
	var DOWN  = Input.is_action_pressed("ui_down")

	# vector (0 or 1)
	move_dir.x = -int(LEFT) + int(RIGHT)
	move_dir.y = -int(UP) + int(DOWN)

	# other keys
	is_pressing_action = Input.is_action_pressed("ui_action")


# =====================
# apply input to motion
func movement_loop():
	var motion = move_dir.normalized() * SPEED
	move_and_slide(motion, Vector2(0, 0))


# ========================================================
# will change sprite direction regarding walking direction
func sprite_loop():
	# sprite facing position
	match move_dir:
		Direction.up:
			sprite_dir = "up"
		Direction.right:
			sprite_dir = "right"
		Direction.left:
			sprite_dir = "left"
		Direction.down:
			sprite_dir = "down"


# =======================================
# will change animation from 2 parameters
func anim_switch(animation_):
	var new_anim = str(animation_, "_", sprite_dir)

	if $anim.current_animation != new_anim:
		$anim.play(new_anim)


# =====================================
# update facing ray,
# will help with detection of objects in front of player
func facing_ray_loop():
	# set raycast direction
	if move_dir != Direction.idle:
		facing_ray.cast_to = move_dir * 10


# ===================================
# activate objects in front of us
func activate_in_front():
	# do we hit something?
	if facing_ray.is_colliding():
		var collider_parent = facing_ray.get_collider().get_parent()
		# is that a door?
		if collider_parent.is_in_group("doors") && collider_parent.has_method('open'):
			if collider_parent.key!="":
				# what key does this door needs?
				var key_found = inventory.search(collider_parent.key)
				if key_found:
					collider_parent.open(key_found)
			else:
				collider_parent.open()


# ====================================
# automatically picking up items in front of us
# (they have to be in "items" group)
func pickUp(item_):
	# add to inventory
	inventory.add(item_)
	
	# testing hud refresh
	hp -= 10
	emit_signal("hp_changed", hp)
