extends Node

# inventory content
var items = []


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass


# add item to inventory
func add(item_):
	# deal with different item types?
	if item_.is_in_group("keys"):
		var key = Key.new(item_.name)
		items.append(key)
	

# search item with a specific name and returns it
# null otherwise
func search(name_):
	for itm in items:
		if itm.name == name_:
			return itm
	
	return null


# ==============================================
# specific items

# ========================
# Keys
class Key:
	
	var name = ""
	
	func _init(name_):
		name = name_