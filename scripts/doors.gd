extends Node2D

const CLOSED = "door_closed"
const OPENED = "door_opened"
const status = CLOSED

# gui settings
export var key = "" # key name that can open that door

var is_animating = false

func _ready():
	# Called when the node is added to the scene for the first time.
	add_to_group("doors")
	$anim.connect("animation_finished", self, "opening_finished")


# signal receiver
func opening_finished():
	is_animating = false

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func open(key_ = null):
	if is_animating || status == OPENED:
		return

	if key!="" && (key_==null || key_.name!=key):
		print("Nope, can't open without the right key")
		return

	# play animation
	$anim.play("open")
	# disable collider to let player pass through
	$StaticBody2D/CollisionShape2D.disabled = true

	# update internal state
	is_animating = true
	status = OPENED


func close():
	# once the door is open, there is no colliding object enabled anymore
	# I need a non-obtrusive target to allow to close the door
	# or something like that
	print("TODO close the door")
	status = CLOSED